# Bionformatics. Task A: Levenshtein distance.
This repository contains solutions for task A: Levenshtein distance between two strings.
- Simple recursive solution;
- Recursive solution with memory;
- Iterative solution (Dynamic programming);