# lev_results = {}

# for i in range(95000):
#     if len(lev_results) <= 99999999999:
#         lev_results[str(i)] = i
#     else:
#         for id, key in enumerate(lev_results.keys()):
#             if id == 0:
#                 lev_results[str(i)] = lev_results.pop(key)
#                 lev_results[str(i)] = i
    
# lev_results = []

# for i in range(10):
#     if len(lev_results) <= 3:
#         lev_results.append(i)
#     else:
#         lev_results[i%len(lev_results)] = i
    
#lev_results[list(lev_results.keys())[0]] = i
import numpy as np

def lev_d(s, t):
    rows = len(s)+1
    cols = len(t)+1
    dist = np.zeros ((rows, cols))
    for i in range(1, rows):
        dist[i][0] = i
    for i in range(1, cols):
        dist[0][i] = i
        
    for col in range(1, cols):
        for row in range(1, rows):
            dist[row][col] = min(dist[row-1][col] + 1,
                                 dist[row][col-1] + 1,
                                 dist[row-1][col-1] + 0 if s[row-1] == t[col-1] else 1)
    
    for r in range(rows):
        print(dist[r])
    print(dist[-1][-1])
     
    return dist[-1][-1]

lev_d('aca', 'aab')