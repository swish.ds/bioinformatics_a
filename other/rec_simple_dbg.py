from collections import Counter
import time

def call_counter(func):
    def helper(*args, **kwargs):
        helper.calls += 1
        key = str(args) + str(kwargs)
        helper.c[key] += 1
        return func(*args, **kwargs)
    helper.c = Counter()
    helper.calls = 0
    helper.__name__= func.__name__

    return helper

@call_counter
def LD(s, t):
    if s == "":
        return len(t)
    if t == "":
        return len(s)
    
    last_s = s[-1]
    last_t = t[-1]

    if last_s == last_t:
        cost = 0
    else:
        cost = 1
    
    delt = LD(s[:-1], t) + 1
    inst = LD(s, t[:-1]) + 1
    subt = LD(s[:-1], t[:-1]) + cost
    val = [delt, inst, subt]

    res = min(val)

    return res

a = 'fasfdsg'
b = 'jytejtweshgfd'

start_time = time.time()
print(LD(a, b))
print("--- %f seconds ---" % (time.time() - start_time))
print("LD() was called " + str(LD.calls) + " times")
#print(LD.c.most_common())