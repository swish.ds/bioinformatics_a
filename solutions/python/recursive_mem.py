file = open('input.txt','r')
strings = file.read().splitlines()

lev_results = {}
def lev_d(a, b):
    if a == "":
        return len(b)
    if b == "":
        return len(a)

    if (a[:-1], b) not in lev_results:
        lev_results[(a[:-1], b)] = lev_d(*(a[:-1], b))
    if (a, b[:-1]) not in lev_results:
        lev_results[(a, b[:-1])] = lev_d(*(a, b[:-1]))
    if (a[:-1], b[:-1]) not in lev_results:
        lev_results[(a[:-1], b[:-1])] = lev_d(*(a[:-1], b[:-1]))

    res = min([lev_results[(a[:-1], b)] + 1,
               lev_results[(a, b[:-1])] + 1,
               lev_results[(a[:-1], b[:-1])] + (0 if a[-1] == b[-1] else 2)])

    return res

def find_min(strings):
    passed = []
    results = []
    for i in range(len(strings)):
        for j in range(len(strings)):
            if strings[i] != strings[j] and strings[j] not in passed:
                results.append([lev_d(strings[i], strings[j]), i + 1, j + 1])
        passed.append(strings[i])
    
    f = open("output.txt","w+")
    f.write('%d %d %d' % (min(results)[1], min(results)[2], min(results)[0]))
    f.close()
    # print(sorted(results))
    # print('%d %d %.2f' % (min(results)[1], min(results)[2], min(results)[0]))

find_min(strings)