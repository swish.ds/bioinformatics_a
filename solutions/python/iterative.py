file = open('input.txt','r')
strings = file.read().splitlines()

def lev_d(a, b):
    cols = len(a)+1
    rows = len(b)+1
    #print(cols, rows)
    dist = [[0 for x in range(cols)] for x in range(rows)]
    #print(len(dist))
    
    for i in range(1, cols):
        dist[0][i] = i
    
    for i in range(1, rows):
        dist[i][0] = i
   
    for row in range(1, rows):
        for col in range(1, cols):
            dist[row][col] = min(dist[row][col-1] + 1,
                                 dist[row-1][col] + 1,
                                 dist[row-1][col-1] + (0 if a[col-1] == b[row-1] else 2))
    
    return dist[-1][-1]

def find_min(strings):
    passed = []
    results = []
    for i in range(len(strings)):
        for j in range(len(strings)):
            if strings[i] != strings[j] and strings[j] not in passed:
                results.append([lev_d(strings[i], strings[j]), i + 1, j + 1])
        passed.append(strings[i])
    
    # print('%d %d %d' % (min(results)[1], min(results)[2], min(results)[0]))
    
    f = open("output.txt","w+")
    f.write('%d %d %d' % (min(results)[1], min(results)[2], min(results)[0]))
    f.close() 
    # # print(sorted(results))
    # print('%d %d %.2f' % (min(results)[1], min(results)[2], min(results)[0]))

find_min(strings)