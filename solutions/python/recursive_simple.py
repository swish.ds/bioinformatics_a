import time

file = open('input.txt','r')
strings = file.read().splitlines()

def lev_d(a, b):
    if a == "":
        return len(b)
    if b == "":
        return len(a)

    res = min([lev_d(a[:-1], b) + 1,
               lev_d(a, b[:-1]) + 1, 
               lev_d(a[:-1], b[:-1]) + (0 if a[-1] == b[-1] else 2)])

    return res

def find_min(strings):
    passed = []
    results = []
    for i in range(len(strings)):
        for j in range(len(strings)):
            if strings[i] != strings[j] and strings[j] not in passed:
                results.append([lev_d(strings[i], strings[j]), i + 1, j + 1])
        passed.append(strings[i])
    
    f = open("output.txt","w+")
    f.write('%d %d %d' % (min(results)[1], min(results)[2], min(results)[0]))
    f.close() 

start = time.time()
find_min(strings)
print("Took %f sec" % (time.time() - start))