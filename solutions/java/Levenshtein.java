import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Levenshtein {

    public static int find_min(int[] arr) {

        int min = arr[0];
        for(int num : arr) {
            if (num < min){
                min = num;
            }
        }
        return min;  
    }
    
    public static int lev_d(String a, String b) {
        int cols = a.length() + 1;
        int rows = b.length() + 1;
        int[][] dist = new int[rows][cols];

        for (int i = 1; i < cols; i++) {
            dist[0][i] = i;
        }

        for (int j = 1; j < rows; j++) {
            dist[j][0] = j;
        }

        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < cols; j++) {
                int[] vals = { dist[i][j - 1] + 1, dist[i - 1][j] + 1,
                        dist[i - 1][j - 1] + ((a.charAt(j - 1) == b.charAt(i - 1)) ? 0 : 2) };

                dist[i][j] = find_min(vals);
            }
        }

        return dist[rows-1][cols-1];

    }

    public static void main(String[] args) throws IOException {

        Scanner inFile1 = new Scanner(new File("input.txt"));
        inFile1.useDelimiter(",\\s*");

        List<String> strings = new ArrayList<String>();

        while (inFile1.hasNext()) {
            strings.add(inFile1.nextLine());
        }

        List<String> passed = new ArrayList<String>();

        int res;
        int seq1 = 0;
        int seq2 = 0;
        int score = 999999;
        for (int i = 0; i < strings.size(); i++) {
            for (int j = 0; j < strings.size(); j++) {
                if(strings.get(i) != strings.get(j) && !passed.contains(strings.get(j))) {
                    
                    res = lev_d(strings.get(i), strings.get(j));
                    if (res < score) {
                        seq1=i + 1; seq2=j + 1; score = res;
                    }
                    else if (res == score && i < seq1) {
                        seq1=i + 1; seq2=j + 1; score = res;
                    }
                    else if (res == score && i == seq1 && j < seq2) {
                        seq1=i + 1; seq2=j + 1; score = res;
                    }
                }
            }
            passed.add(strings.get(i));
        }

        ////System.out.println(seq1 + " " + seq2  + " " + score);
        
        FileWriter output = new FileWriter ("output.txt");
        output.write(seq1 + " " + seq2  + " " + score);
        output.close();

    }
}